﻿using api1.shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api2.Controllers
{
    [ApiController]   
    public class HomeController : ControllerBase
    {
        private readonly IApiService _api1;

        public HomeController(IApiService api1)
        {
            _api1 = api1;
        }

        [HttpGet("/")]
        public IActionResult Hello()
        {
            return Ok(_api1.GetMessage());
        }
    }
}
