﻿using System;

namespace api1.shared
{
    public interface IApiService
    {
        string GetMessage();
    }
    public class Api1Service : IApiService
    {
        public string GetMessage()
        {
            return "Hello, world!";
        }
    }
    public class Api2Service : IApiService
    {
        public string GetMessage()
        {
            return "Hello, everyone!";
        }
    }
}
