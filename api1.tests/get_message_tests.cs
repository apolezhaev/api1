using api1.shared;
using NUnit.Framework;

namespace api1.tests
{
    public class get_message_tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test] 
        public void get_message_test()
        {
            Assert.AreEqual("Hello, everyone!", new Api2Service().GetMessage());
        }
    }
}